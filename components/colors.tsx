export const colors = {
    primary: '#1F69FF',
    inheritPrimary: '#E9F0FF',
    neutral20: '#F5F5F5',
    neutral40: '#E0E0E0',
    neutral90: '#404040',
    neutral60: '#9E9E9E',
    neutral10: '#FFFFFF',
    neutral100: '#0A0A0A',
    neutral70: '#757575',
    neutral80: '#616161',
    bg: '#fff',
  };