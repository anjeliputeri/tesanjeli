import { useNavigation } from "@react-navigation/native";
import React, { FunctionComponent, useState } from "react";
import { View, StyleSheet, Image, Text, TouchableOpacity } from "react-native";
import { colors } from "../components/colors";
import OTPInputField from "../components/OTPInputField";


const Pin: FunctionComponent = () => {
    const navigation = useNavigation();
    
    const handlePin = ()=>{
        navigation.navigate('Otp' as never);
    };

    const [passcode, setPasscode] = useState<string[]>(['', '', '', '', '', '']);
  
    const handleNumberPress = (number: string) => {
      const newPasscode = [...passcode];
      const emptyIndex = newPasscode.indexOf('');
      if (emptyIndex !== -1) {
        newPasscode[emptyIndex] = number;
        setPasscode(newPasscode);
      }
    };
  
    const handleDeletePress = () => {
      const newPasscode = [...passcode];
      const emptyIndex = newPasscode.indexOf('');
      if (emptyIndex !== 6) {
        if (emptyIndex === -1) {
          newPasscode[newPasscode.length - 1] = '';
        } else {
          newPasscode[emptyIndex - 1] = '';
        }
        setPasscode(newPasscode);
      }
    };

  return (
    <View style={styles.container}>
        <TouchableOpacity onPress={handlePin}>
        <Image style={styles.icon} source={require("./../assets/ic_back.png")}/>
        </TouchableOpacity>
      <Text style={styles.pin}>Masukkan PIN</Text>
      <View style={styles.codeContainer}>
        {passcode.map((p, index) => {
          let style = p !== '' ? styles.code2 : styles.code1;
          return <View key={index} style={style}></View>;
        })}
      </View>
      <View style={styles.numberContainer}>
        {[...Array(9)].map((_, index) => (
          <TouchableOpacity key={index} style={styles.number} onPress={() => handleNumberPress(String(index + 1))}>
            <Text style={styles.numberText}>{index + 1}</Text>
          </TouchableOpacity>
        ))}
        <View style={styles.number}>
          <Text style={styles.numberText}></Text>
        </View>
        <TouchableOpacity style={styles.number} onPress={() => handleNumberPress('0')}>
          <Text style={styles.numberText}>0</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.number} onPress={handleDeletePress}>
          <Image style={styles.delete} source={require("./../assets/ic_delete.png")}/>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  icon: {
    width: 24,
    height: 24,
    position: "absolute",
    top: 40,
    left: 10
  },
  pin: {
    fontSize: 16,
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: 150,
    color: colors.neutral10,
  },
  codeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  code1: {
    width: 13,
    height: 13,
    borderRadius: 13,
    borderWidth: 1,
    borderColor: 'white',
    marginHorizontal: 5
  },
  code2: {
    width: 13,
    height: 13,
    borderRadius: 13,
    backgroundColor: 'white',
    marginHorizontal: 5
  },
  number: {
    width: 75,
    height: 75,
    marginTop: 5,
    margin: 22,
    justifyContent: 'center',
    alignItems: 'center'
  },
  numberContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 70,
    alignItems: 'center',
    justifyContent: 'center'
  },
  numberText: {
    fontSize: 36,
    color: colors.neutral10,
    letterSpacing: 0,
    textAlign: 'center'
  },
  delete: {
    height: 36,
    width: 36,
  },
});

export default Pin;
