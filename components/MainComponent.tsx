import { FunctionComponent } from 'react';
import styled from 'styled-components/native';
import { colors } from './colors';
import { ContainerProps } from './types';

const { bg } = colors;

const StyledView = styled.View`
  flex: 1;
  padding: 25px;
  padding-top: 40px;
  background-color: ${bg};
  align-items: center;
`;

const Styled = styled.View`
flex: 1;
padding: 25px;
padding-top: 40px;
background-color: ${colors.primary};
align-items: center;
`;

const MainContainer: FunctionComponent<ContainerProps> = (props) => {
  return <StyledView style = {props.style}>{props.children}</StyledView>
};

export default MainContainer;