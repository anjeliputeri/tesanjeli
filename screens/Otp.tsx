import React, { FunctionComponent, useState, useEffect} from "react";
import MainContainer from "../components/MainComponent";
import { Text, StyleSheet, View, Image, Pressable, Keyboard, TouchableOpacity} from "react-native";
import { colors } from "../components/colors";

import OTPInputField from "../components/OTPInputField";
import { useNavigation } from "@react-navigation/native";

const Otp: FunctionComponent = () => {
  const [code, setCode] = useState("");
  const [pinReady, setPinReady] = useState(false);
  const MAX_CODE_LENGTH = 4;

  const navigation = useNavigation();
  const handleOtp = () => {
    navigation.navigate('Login' as never);
  };

  useEffect(() => {
    const delay = 6000;
    const timer = setTimeout(() =>{
      navigation.navigate('Pin' as never); 
    }, delay);

    return () => clearTimeout(timer);
  }, [navigation]);
  

  return (
    <Pressable style={styles.container} onPress={Keyboard.dismiss}>
    <MainContainer style={false}>
      <View style={styles.background}>
        <TouchableOpacity onPress={handleOtp}>
        <Image style={styles.icon} source={require("./../assets/ic_back.png")}/>
        </TouchableOpacity>
        <Text style={styles.textMasuk}>Masukan Kode OTP</Text>
      </View>
      <Image style={styles.imgOtp} source={require("./../assets/ic_otp.png")}/>
      <Text style={styles.textOtp}>Kode OTP Sudah Dikirim! </Text>
      <Text style={styles.cek}>Silahkan cek WhatsApp kamu!
      {'\n'}Jika kode OTP belum terkirim, klik “Kirim Ulang” </Text>
      <OTPInputField
      setPinReady={setPinReady}
      code={code}
      setCode={setCode}
      maxLength={MAX_CODE_LENGTH}/>
      <Text style={styles.kirim}>
        Belum dapat kode?<Text style={{color: colors.primary}}> (Tunggu 15s)</Text>
      </Text>
    </MainContainer>
    </Pressable>
  );
};

export default Otp;

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  background: {
    position: 'absolute',
    zIndex: -10,
    backgroundColor: colors.primary,
    width:410,
    height: 80,
    display: 'flex',
    flexDirection: 'row',
    columnGap: 10,
    alignItems: 'center'
  },
  textMasuk: {
    color: colors.neutral10,
    marginLeft: 10,
    fontWeight: '600',
    fontSize: 18,
    top:10
  },
  icon: {
    width: 24,
    height: 24,
    left: 10,
    top:10
  },
  imgOtp: {
    width: 192,
    height: 192,
    marginTop: 40
  },
  textOtp: {
    color: colors.neutral100,
    fontWeight: '600',
    fontSize: 20  
  },
  cek: {
    justifyContent: 'center',
    textAlign: 'center',
    color: colors.neutral70,
    marginTop: 10,
    lineHeight: 21,
    fontSize: 14
  },
  kirim: {
    color: colors.neutral80,
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 21
  }
});
