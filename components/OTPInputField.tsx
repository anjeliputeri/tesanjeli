import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import styled from "styled-components/native";
import { colors } from "./colors";
import { TextInput } from "react-native";

interface OTPInputFieldProps {
  setPinReady: React.Dispatch<React.SetStateAction<boolean>>;
  code: string;
  setCode: React.Dispatch<React.SetStateAction<string>>;
  maxLength: number;
}

export const OTPInputSection = styled.View`
  justify-content: center;
  align-items: center;
  margin-vertical: 30px;
`;

export const HiddenTextInput = styled.TextInput`
position: absolute;
width: 1px;
height: 1px;
opacity: 0;
`;

export const OTPInputContainer = styled.Pressable`
  width: 70%;
  flex-direction: row;
  justify-content: space-around;
`;

export const OTPInput = styled.View`
  min-width: 15%;
  border-radius: 12px;
  padding: 12px;
  background-color: ${colors.inheritPrimary};
  color: ${colors.primary};
`;

export const OTPInputText = styled.Text`
  font-size: 22px;
  font-weight: bold;
  text-align: center;
  color: ${colors.primary};
`;

export const OTPInputFocused = styled(OTPInput)`
    border-color: ${colors.primary};
    border-width: 2px;
    `;

const OTPInputField: FunctionComponent<OTPInputFieldProps> = ({
  setPinReady,
  code,
  setCode,
  maxLength,
}) => {
  const codeDigitsArray = new Array(maxLength).fill(0);

  const textInputRef = useRef<TextInput>(null);

  const [inputContainerIsFocused, setInputContainerIsFocused] =
    useState(false);

  const handleOnPress = () => {
    setInputContainerIsFocused(true);
    textInputRef?.current?.focus();
  };

  const handleOnBlur = () => {
    setInputContainerIsFocused(false);
  };

  useEffect(() => {
    setPinReady(code.length === maxLength);
    return () => setPinReady(false);
  }, [code]);

  const toCodeDigitInput = (_value: any, index: number) => {
    const emptyInputChar = " ";
    const digit = code[index] || emptyInputChar;

    const isCurrentDigit = index === code.length;
    const isLastDigit = index === maxLength - 1;
    const isCodeFull = code.length === maxLength;

    const isDigitFocused = isCurrentDigit || (isLastDigit &&
        isCodeFull);

    const StyledOTPInput = 
    inputContainerIsFocused && isDigitFocused ? OTPInputFocused :
    OTPInput;

    return (
      <StyledOTPInput key={index}>
        <OTPInputText>{digit}</OTPInputText>
      </StyledOTPInput>
    );
  };

  return (
    <OTPInputSection>
      <OTPInputContainer onPress={handleOnPress}>
        {codeDigitsArray.map(toCodeDigitInput)}
      </OTPInputContainer>
      <HiddenTextInput
        value={code}
        onChangeText={setCode}
        maxLength={maxLength}
        keyboardType="number-pad"
        returnKeyType="done"
        textContentType="oneTimeCode"
        ref={textInputRef}
        onBlur={handleOnBlur}
      />
    </OTPInputSection>
  );
};

export default OTPInputField;
