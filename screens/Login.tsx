import React, { FunctionComponent } from "react";
import { StatusBar } from 'expo-status-bar';
import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import MainContainer from './../components/MainComponent';
import { colors } from './../components/colors';
import { useNavigation } from "@react-navigation/native";


const Login: FunctionComponent = () => {
  const navigation = useNavigation();

  const handleSignIn = () => {
    navigation.navigate('Otp' as never);
  };

  
  
  return (
    <MainContainer style={false}>
      <StatusBar backgroundColor={colors.primary} />
      <Image style={styles.logo} source={require("./../assets/logo.png")} />
      <Image style={styles.background} source={require("./../assets/bg_bangbeli.png")} />
      <Text style={styles.heading}>Mari bergabung dengan
        <Text style={{ fontWeight: 'bold' }}> 13.000+ </Text>
        Mitra{'\n'}Bangbeli yang tersebar di
        <Text style={{ fontWeight: 'bold' }}> 11 </Text>
        Provinsi{'\n'}Indonesia.</Text>
      <View>
        <Text style={styles.label}>Masukan No WhatsApp</Text>
        <View style={styles.inputBox}>
          <TextInput style={styles.input}
            placeholderTextColor={colors.neutral60}
            placeholder='0812********' />
        </View>
      </View>
      <View style={styles.alert}>
        <Image style={styles.icon} source={require("./../assets/ic_alert.png")} />
        <Text style={styles.textAlert}>Masukan nomor WhatsApp yang terdaftar dalam{'\n'}aplikasi Bangbeli</Text>
      </View>
      <TouchableOpacity style={styles.btnSignIn} onPress={handleSignIn}>
        <Text style={styles.textSignIn}>Masuk</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btnSignUp}>
        <Text style={styles.textSignUp}>Daftar</Text>
      </TouchableOpacity>
    </MainContainer>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 150,
    height: 50,
    top: 32,
    right: 110
  },
  background: {
    position: 'absolute',
    zIndex: -10,
    resizeMode: 'center',
    bottom: -200,
    width: 500
  },
  heading: {
    marginTop: 32,
    fontSize: 16,
    fontWeight: 'normal',
    lineHeight: 22,
    color: 'white',
    right: 45
  },
  label: {
    marginTop: 160,
    fontSize: 16,
    color: 'black',
    fontWeight: '600'
  },
  input: {
    fontSize: 16,
  },
  inputBox: {
    backgroundColor: colors.inheritPrimary,
    width: 365,
    height: 55,
    borderRadius: 12,
    paddingVertical: 12,
    paddingHorizontal: 16,
    marginTop: 8
  },
  alert: {
    backgroundColor: colors.neutral20,
    display: 'flex',
    flexDirection: 'row',
    columnGap: 10,
    marginTop: 16,
    width: 365,
    height: 77,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 10,
    alignItems: 'center',
    borderColor: colors.neutral40,
    borderWidth: 1
  },
  icon: {
    width: 24,
    height: 24
  },
  textAlert: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
    color: colors.neutral90
  },
  btnSignIn: {
    backgroundColor: colors.primary,
    width: 365,
    height: 50,
    borderRadius: 24,
    marginTop: 40,
    alignItems: 'center',
    paddingVertical: 10
  },
  textSignIn: {
    color: 'white',
    fontWeight: '600',
    fontSize: 20
  },
  btnSignUp: {
    borderColor: colors.primary,
    borderWidth: 1,
    width: 365,
    height: 50,
    borderRadius: 24,
    marginTop: 12,
    alignItems: 'center',
    paddingVertical: 10
  },
  textSignUp: {
    color: colors.primary,
    fontWeight: '600',
    fontSize: 20
  },
});
