import React, { FunctionComponent } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "../screens/Login";
import Otp from "../screens/Otp";
import Pin from "../screens/Pin";

export type RootStackParamList = {
  Login: undefined;
  Otp: undefined;
  Pin: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

const RootStack: FunctionComponent = () => {
  return (
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Pin"
          component={Pin}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
  );
};

export default RootStack;
